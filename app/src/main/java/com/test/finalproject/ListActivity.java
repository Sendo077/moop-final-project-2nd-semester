package com.test.finalproject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ListActivity extends Activity {

    ArrayList<String> items;
    ArrayAdapter<String> arrayAdapter;

    ListView listview;

    EditText input;
    Button btn;

    TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Intent intent  = getIntent();
//
        Date date = new Date();
        String currDate = new SimpleDateFormat("MMM dd, YYY", Locale.getDefault()).format(date);
        tvDate = findViewById(R.id.currDate);
        tvDate.setText(currDate);

        items = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(this, R.layout.list_view_layout, items);

        listview = findViewById(R.id.listItem);

        listview.setAdapter(arrayAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                textView.setText("I've done this :)");
                textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        });

        input = findViewById(R.id.write);
        btn = findViewById(R.id.add);
    }

    public void writeList(View view) {
        items.add(input.getText().toString());

        // notifikasi data muncul
        arrayAdapter.notifyDataSetChanged();

        // biar aman aja
        input.setText("");

        Toast toast = Toast.makeText(this, "New List Added", Toast.LENGTH_SHORT);
        toast.show();
    }

}
